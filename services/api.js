import axios from 'axios'

export const api = axios.create({
  baseURL: process.env.PRO_API_URL,
})

export const catalogApi = axios.create({
  baseURL: process.env.CATALOG_API_URL,
})
