require('dotenv').config({ path: `.env.${process.env.NODE_ENV}` })

export default {
  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
   */
  mode: 'spa',
  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */
  head: {
    htmlAttrs: {
      lang: 'pt-br',
    },
    titleTemplate: (titleChunk) => {
      return titleChunk
        ? `${titleChunk} - Avalie seu carro`
        : 'Avalie seu carro'
    },
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1, viewport-fit=cover',
      },
      {
        hid: 'description',
        name: 'description',
        content:
          'Quer saber quanto vale seu carro hoje? Estamos aqui para te ajudar! Preencha as informações e receba uma avaliação na hora.',
      },
      {
        hid: 'og:description',
        name: 'og:description',
        content:
          'Quer saber quanto vale seu carro hoje? Estamos aqui para te ajudar! Preencha as informações e receba uma avaliação na hora.',
      },
      {
        hid: 'twitter:description',
        name: 'twitter:description',
        content:
          'Quer saber quanto vale seu carro hoje? Estamos aqui para te ajudar! Preencha as informações e receba uma avaliação na hora.',
      },
      {
        hid: 'og:site_name',
        property: 'og:site_name',
        content: 'Avalie seu carro',
      },
      {
        hid: 'og:title',
        property: 'og:title',
        content: 'Avalie seu carro',
      },
      {
        hid: 'twitter:title',
        name: 'twitter:title',
        content: 'Avalie seu carro',
      },
      {
        hid: 'og:type',
        property: 'og:type',
        content: 'website',
      },
      {
        hid: 'og:image',
        property: 'og:image',
        content: 'http://dryve.com.br/favicon.png',
      },
      {
        hid: 'twitter:image',
        name: 'twitter:image',
        content: 'http://dryve.com.br/favicon.png',
      },
    ],
    link: [{ rel: 'icon', type: 'image/png', href: 'favicon.png' }],
  },
  /*
   ** Global CSS
   */
  css: ['~/styles/helpers.scss', '~/styles/fonts.scss', '~/styles/global.scss'],
  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: [{ src: '~/plugins/vue-the-mask.js' }],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: true,
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    '@nuxtjs/dotenv',
    // ['@nuxtjs/dotenv', { filename: `.env.${process.env.NODE_ENV}` }],
    '@nuxtjs/eslint-module',
    '@nuxtjs/stylelint-module',
    '@nuxtjs/vuetify',
    '@nuxtjs/gtm',
  ],
  gtm: {
    id: 'GTM-5F76ZVJ',
    pageTracking: process.env.NODE_ENV === 'production',
  },
  /*
   ** Nuxt.js modules
   */
  modules: ['@nuxtjs/axios'],
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    treeShake: {
      directives: ['Touch'],
    },
    customVariables: ['~/styles/colors.scss', '~/styles/vuetify.scss'],
    theme: {
      themes: {
        light: {
          primary: '#0065ff',
          error: '#dc3545',
        },
      },
    },
  },
  env: {
    PRO_API_URL: process.env.PRO_API_URL,
    CATALOG_API_URL: process.env.CATALOG_API_URL,
    KBB_ORIGIN_ID: process.env.KBB_ORIGIN_ID,
    DRYVE_STORE_ID: process.env.DRYVE_STORE_ID,
  },
  /**
   * Gitlab
   */
  router: {
    base: '/avalie-seu-carro/',
  },
  generate: {
    dir: 'public',
  },
}
