export const rules = {
  required: (v) => !!v || 'Campo obrigatório',
  email: (v) => /.+@.+\..+/.test(v) || 'E-mail inválido',
  phone: (v) =>
    /(?:\()[0-9]{2}(?:\))\s?[0-9]{4,5}(?:-)[0-9]{4}$/gm.test(v) ||
    'Celular inválido',
}
