export default function mapProperties({ prefix, properties }) {
  return {
    ...Object.keys(properties).reduce(
      (objProperties, key) => ({
        ...objProperties,
        [key]: (prefix ? prefix.concat('/') : '').concat(key),
      }),
      {}
    ),
  }
}
