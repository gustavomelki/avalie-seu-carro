import { mutationAliases } from './mutations'
import { api } from '@/services/api'
import { mapProperties } from '@/utils'

const actions = {
  getIntentResult: async ({ commit }, payload) => {
    try {
      const { data } = await api.get(
        `/public/api/v1/white-label/intentions/sales/${payload.intention_sale_id}/values`
      )
      return commit(mutationAliases.setIntentResult, data)
    } catch (err) {
      console.error(err.response)
    }
  },
}

export default actions

export const actionTypes = mapProperties({
  prefix: 'intent',
  properties: actions,
})
