import { mapProperties } from '@/utils'

const getters = {
  getIntentResult: (state) => state.result,
  getIntentSale: (state) => state.sale,
  getIntentSaleBrand: (state) => state.sale.brand,
  getIntentSaleModel: (state) => state.sale.model,
  getIntentSaleVersion: (state) => state.sale.version,
}

export default getters

export const getterTypes = mapProperties({
  prefix: 'intent',
  properties: getters,
})
