import Vue from 'vue'
import { mapProperties } from '@/utils'

const mutations = {
  setIntentResult: (state, payload) => {
    Object.keys(payload).forEach((item, index) => {
      Vue.set(state.result, item, Object.values(payload)[index])
    })
  },
  setIntentSale: (state, payload) => {
    Object.keys(payload).forEach((item, index) => {
      Vue.set(state.sale, item, Object.values(payload)[index])
    })
  },
  clearIntentSale: (state) => {
    state.sale = {}
  },
}

export default mutations

export const mutationTypes = mapProperties({
  prefix: 'intent',
  properties: mutations,
})

export const mutationAliases = mapProperties({
  properties: mutations,
})
