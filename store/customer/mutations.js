import Vue from 'vue'
import { mapProperties } from '@/utils'

const mutations = {
  setCustomer: (state, payload) => {
    Object.keys(payload).forEach((item, index) => {
      Vue.set(state, item, Object.values(payload)[index])
    })
  },
}

export default mutations

export const mutationTypes = mapProperties({
  prefix: 'customer',
  properties: mutations,
})

export const mutationAliases = mapProperties({
  properties: mutations,
})
