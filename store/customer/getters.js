import { mapProperties } from '@/utils'

const getters = {
  getCustomer: (state) => state,
  getCustomerFirstName: (state) => state.first_name,
  getCustomerId: (state) => state.id,
}

export default getters

export const getterTypes = mapProperties({
  prefix: 'customer',
  properties: getters,
})
