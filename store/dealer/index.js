import { getterTypes as getters } from './getters'
import { mutationTypes as mutations } from './mutations'
import { actionTypes as actions } from './actions'

export default {
  module: 'dealer',
  getters,
  mutations,
  actions,
}
