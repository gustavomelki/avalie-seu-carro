import { mutationAliases } from './mutations'
import { mapProperties } from '@/utils'
import { api } from '@/services/api'

const actions = {
  getDealer: ({ commit }, storeId) => {
    const dealerId = !storeId ? process.env.DRYVE_STORE_ID : storeId
    return new Promise((resolve, reject) => {
      api
        .get(`/public/api/v1/white-label/stores/${dealerId}/configuration`)
        .then((res) => {
          res.data.store_id = dealerId
          commit(mutationAliases.setDealer, res.data)
          resolve(res.data)
        })
        .catch((err) => reject(err))
    })
  },
}

export default actions

export const actionTypes = mapProperties({
  prefix: 'dealer',
  properties: actions,
})
