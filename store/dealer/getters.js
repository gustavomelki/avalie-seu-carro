import { mapProperties } from '@/utils'

const getters = {
  getDealer: (state) => state,
}

export default getters

export const getterTypes = mapProperties({
  prefix: 'dealer',
  properties: getters,
})
