export const state = () => ({
  bodiesByModel: [],
  brands: [],
  fuels: [],
  modelsByBrand: [],
  optionalsByModel: [],
  transmissions: [],
  versionsByModel: [],
  yearsByModel: [],
})

export default state
