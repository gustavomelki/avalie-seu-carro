import { mutationAliases } from './mutations'
import { catalogApi } from '@/services/api'
import { mapProperties } from '@/utils'
import EventBus from '@/utils/eventbus'

const actions = {
  getBodiesByModel: async ({ commit }, payload) => {
    try {
      const { data } = await catalogApi.get(
        `/public/api/v1/catalog/${process.env.KBB_ORIGIN_ID}/brands/${payload.brandId}/models/${payload.modelId}/bodies?page=0&size=300`
      )
      return commit(mutationAliases.setBodiesByModel, data.content)
    } catch (err) {
      console.error(err.response)
    }
  },
  getBrands: async ({ commit }) => {
    try {
      const { data } = await catalogApi.get(
        `public/api/v1/catalog/${process.env.KBB_ORIGIN_ID}/brands?page=0&size=300`
      )
      return commit(mutationAliases.setBrands, data.content)
    } catch (err) {
      console.error(err.response)
    }
  },
  getFuels: async ({ commit }) => {
    try {
      const { data } = await catalogApi.get(
        `public/api/v1/catalog/fuels?page=0&size=300`
      )
      return commit(mutationAliases.setFuels, data.content)
    } catch (err) {
      console.error(err.response)
    }
  },
  getModelsByBrand: async ({ commit }, brandId) => {
    try {
      const { data } = await catalogApi.get(
        `/public/api/v1/catalog/${process.env.KBB_ORIGIN_ID}/brands/${brandId}/models?page=0&size=300`
      )
      return commit(mutationAliases.setModelsByBrand, data.content)
    } catch (err) {
      console.error(err.response)
    }
  },
  getOptionalsByModel: async ({ commit }, payload) => {
    try {
      const { data } = await catalogApi.get(
        `/public/api/v1/catalog/vehicles/${payload}/equipments/optionals?page=0&size=300`
      )
      if (!data.length) {
        EventBus.$emit('totalCarSteps', 3)
      } else {
        EventBus.$emit('totalCarSteps', 4)
      }
      return commit(mutationAliases.setOptionalsByModel, data)
    } catch (err) {
      console.error(err.response)
    }
  },
  getTransmissions: async ({ commit }) => {
    try {
      const { data } = await catalogApi.get(
        `public/api/v1/catalog/transmissions?page=0&size=300`
      )
      return commit(mutationAliases.setTransmissions, data.content)
    } catch (err) {
      console.error(err.response)
    }
  },
  getVersionsByModel: async ({ commit }, payload) => {
    try {
      const { data } = await catalogApi.get(
        `/public/api/v1/catalog/${process.env.KBB_ORIGIN_ID}/brands/${payload.brandId}/models/${payload.modelId}/versions?page=0&size=300&modelYear=${payload.year}`
      )
      return commit(mutationAliases.setVersionsByModel, data.content)
    } catch (err) {
      console.error(err.response)
    }
  },
  getYearsByModel: async ({ commit }, payload) => {
    if (!payload.brandId || !payload.modelId) return
    try {
      const { data } = await catalogApi.get(
        `/public/api/v1/catalog/${process.env.KBB_ORIGIN_ID}/brands/${payload.brandId}/models/${payload.modelId}/years?page=0&size=300`
      )
      return commit(mutationAliases.setYearsByModel, data.content)
    } catch (err) {
      console.error(err.response)
    }
  },
}

export default actions

export const actionTypes = mapProperties({
  prefix: 'catalog',
  properties: actions,
})
