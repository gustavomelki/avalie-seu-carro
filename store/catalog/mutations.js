import Vue from 'vue'
import { mapProperties } from '@/utils'

const mutations = {
  setBodiesByModel: (state, payload) => (state.bodiesByModel = payload),
  setBrands: (state, payload) => (state.brands = payload),
  setFuels: (state, payload) => (state.fuels = payload),
  setModelsByBrand: (state, payload) => (state.modelsByBrand = payload),
  setOptionalsByModel: (state, payload) => (state.optionalsByModel = payload),
  setTransmissions: (state, payload) => (state.transmissions = payload),
  setVersionsByModel: (state, payload) => (state.versionsByModel = payload),
  setYearsByModel: (state, payload) => (state.yearsByModel = payload),
  clearCatalogFrom: (state, skip) => {
    Object.keys(state).forEach((item) => {
      if (!skip || !skip.includes(item)) {
        if (item !== 'fuels' && item !== 'transmissions') {
          Vue.set(state, item, [])
        }
      }
    })
  },
}

export default mutations

export const mutationTypes = mapProperties({
  prefix: 'catalog',
  properties: mutations,
})

export const mutationAliases = mapProperties({
  properties: mutations,
})
