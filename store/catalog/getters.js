import { mapProperties } from '@/utils'

const getters = {
  getBodiesByModel: (state) => state.bodiesByModel,
  getBrands: (state) => state.brands,
  getFuels: (state) => state.fuels,
  getModelsByBrand: (state) => state.modelsByBrand,
  getOptionalsByModel: (state) => state.optionalsByModel,
  getTransmissions: (state) => state.transmissions,
  getVersionsByModel: (state) => state.versionsByModel,
  getYearsByModel: (state) => state.yearsByModel,
}

export default getters

export const getterTypes = mapProperties({
  prefix: 'catalog',
  properties: getters,
})
