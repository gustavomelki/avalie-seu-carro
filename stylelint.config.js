module.exports = {
  extends: ['stylelint-config-standard', 'stylelint-config-prettier'],
  rules: {
    'color-no-invalid-hex': true,
    'block-no-empty': null,
    'unit-whitelist': ['px', 'vh', 'vw', '%'],
    'at-rule-no-unknown': null,
  },
}
